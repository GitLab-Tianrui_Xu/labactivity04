//2238329 Tianrui Xu
package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;
public class Vector3dTests {
    @Test
    public void getMethodsTests() {
        Vector3d vector = new Vector3d(2, 3, 4);
        assertEquals("x value", 2, vector.getX(), 0);
        assertEquals("y value", 3, vector.getY(), 0);
        assertEquals("z value", 4, vector.getZ(), 0);
    }

    @Test
    public void magnitudeTest() {
        Vector3d vector = new Vector3d(2, 2, 2);
        assertEquals("magnitude test", Math.sqrt(12), vector.magnitude(), 0);
    }

    @Test
    public void dotProductTest() {
        Vector3d vector1 = new Vector3d(2, 2, 2);
        Vector3d vector2 = new Vector3d(4, 4, 4);
        assertEquals("doc product test", 24, vector1.dotProduct(vector2), 0);
    }

    @Test
    public void addTest() {
        Vector3d vector1 = new Vector3d(2, 3, 4);
        Vector3d vector2 = new Vector3d(5, 6, 7);
        Vector3d returnVector = vector1.add(vector2);
        assertEquals("x value", 7, returnVector.getX(), 0);
        assertEquals("y value", 9, returnVector.getY(), 0);
        assertEquals("z value", 11, returnVector.getZ(), 0);
    }
}